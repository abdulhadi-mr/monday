FROM centos:7

RUN yum -y update && yum clean all

RUN yum -y install httpd && \
    yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
    yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm

RUN yum -y install yum-utils && \
    yum-config-manager --enable remi-php74

 RUN yum -y update && \
     yum -y install php php-cli php-mysqlnd php-pdo php-gd php-ldap php-odbc php-pear php-xml php-xmlrpc php-mbstring php-soap curl curl-devel

COPY script.sh .
RUN chmod +x script.sh

WORKDIR /var/www/html
COPY --chown=apache:apache advanced /var/www/html/advanced

WORKDIR /var/www/html/advanced

CMD [ "/script.sh" ]   
