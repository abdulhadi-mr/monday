#!/bin/bash

php init --env=Development --overwrite=All

cat > common/config/main-local.php <<EOF
<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=${MYSQL_HOST};dbname=${MYSQL_DATABASE}',
            'username' => '${MYSQL_USER}',
            'password' => '${MYSQL_PASSWORD}',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],

    ],
];

EOF

php yii migrate --interactive=0 

/usr/sbin/httpd -DFOREGROUND